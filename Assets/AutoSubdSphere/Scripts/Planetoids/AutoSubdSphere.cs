﻿using UnityEngine;

namespace AutoSubdSphere
{
    public class AutoSubdSphere
    {
        public AutoSubdSphere(string name)
        {
            ChunkBall = new ChunkBall();
            GameObject = new GameObject(name);

            MeshFilter filter = GameObject.AddComponent<MeshFilter>();
            filter.mesh = ChunkBall.Mesh;

            GameObject.AddComponent<MeshRenderer>();
        }

        public ChunkBall ChunkBall { get; private set; }
        public GameObject GameObject { get; set; }

        public void SetMaterial(Material material)
        {
            GameObject.GetComponent<MeshRenderer>().material = material;
        }
    }
}