namespace Railmaker
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(MeshGenerator), false)]
    [CanEditMultipleObjects]
    [InitializeOnLoad]
    public abstract class MeshGeneratorEditor : Editor
    {
        internal MeshGenerator MeshGenerator { get; set; }

        /*public void OnEnable ()
        {
            SceneView.onSceneGUIDelegate -= _OnSceneGUI;
            SceneView.onSceneGUIDelegate += _OnSceneGUI;
        }*/

        public override void OnInspectorGUI()
        {
            TitleGui();
            Undo.RecordObject(MeshGenerator, "MeshGenerator Manipulation");
            Undo.ClearUndo(MeshGenerator);
            EditorGUILayout.Space();
        }

        public virtual void OnSceneGUI()
        {

        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Mesh Generator", style);
        }

        #endregion Inspector
    }
}