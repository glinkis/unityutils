﻿using UnityEngine;

namespace AutoSubdSphere
{
	public class Main : MonoBehaviour
	{
	    private AutoSubdSphere planetoid;

	    private void Start()
	    {
            planetoid = new AutoSubdSphere("SubdSurface");
	    }

	    private void Update()
	    {
	        if (Input.GetKeyDown(KeyCode.KeypadPlus))
                planetoid.ChunkBall.UpResAll();

	        if (Input.GetKeyDown(KeyCode.KeypadMinus))
                planetoid.ChunkBall.DownResAll();
	    }
	}
}