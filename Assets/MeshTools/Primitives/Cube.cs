﻿using UnityEngine;

namespace MeshTools.Primitives
{
    public class Cube
    {
        public readonly Mesh Mesh = new Mesh();

        public Cube()
        {
            Build(.5f, .5f, .5f);
        }

        public Cube(float length, float width, float height)
        {
            Build(length, width, height);
        }

        private void Build(float length, float width, float height)
        {
            #region Vertices

            Vector3 p0 = new Vector3(-length, -width, height);
            Vector3 p1 = new Vector3(length, -width, height);
            Vector3 p2 = new Vector3(length, -width, -height);
            Vector3 p3 = new Vector3(-length, -width, -height);

            Vector3 p4 = new Vector3(-length, width, height);
            Vector3 p5 = new Vector3(length, width, height);
            Vector3 p6 = new Vector3(length, width, -height);
            Vector3 p7 = new Vector3(-length, width, -height);

            Mesh.vertices = new Vector3[]
            {
                p0, p1, p2, p3, // Bottom      
                p7, p4, p0, p3, // Left
                p4, p5, p1, p0, // Front
                p6, p7, p3, p2, // Back
                p5, p6, p2, p1, // Right
                p7, p6, p5, p4, // Top
            };

            #endregion

            #region UVs

            Vector2 p00 = new Vector2(0f, 0f);
            Vector2 p10 = new Vector2(1f, 0f);
            Vector2 p01 = new Vector2(0f, 1f);
            Vector2 p11 = new Vector2(1f, 1f);

            Mesh.uv = new Vector2[]
            {
                p11, p01, p00, p10, // Bottom
                p11, p01, p00, p10, // Left
                p11, p01, p00, p10, // Front
                p11, p01, p00, p10, // Back
                p11, p01, p00, p10, // Right
                p11, p01, p00, p10, // Top
            };

            #endregion

            #region Triangles
            Mesh.triangles = new int[]
            {
                // Bottom
                3, 1, 0,
                3, 2, 1,		
 
                // Left
                7, 5, 4,
                7, 6, 5,
 
                // Front
                11, 9, 8,
                11, 10, 9,
 
                // Back
                15, 13, 12,
                15, 14, 13,
 
                // Right
                19, 17, 16,
                19, 18, 17,
 
                // Top
                23, 21, 20,
                23, 22, 21,
            };

            #endregion

            Mesh.RecalculateBounds();
            Mesh.RecalculateNormals();
        }
    }
}