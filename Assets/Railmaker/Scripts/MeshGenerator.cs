﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using Object = System.Object;

namespace Railmaker {

    [Serializable]
    [RequireComponent(typeof (MeshFilter))]
    [RequireComponent(typeof (MeshRenderer))]
    public abstract class MeshGenerator : MonoBehaviour, INotifyPropertyChanged {

        internal string MeshName { get; set; }

        internal int PointCount { get; set; }

        internal List<Vector3> Vertices { get; set; }

        internal List<Vector2> Uvs { get; set; }

        internal List<int> Indices { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void Awake() {
        }

        public virtual void OnEnable() {
            Debug.Log("Enabled");
        }

        public void RebuildMesh() {
            ResetMeshData();
            RecalculateVertices();
            RecalculateIndices();

            Mesh body = new Mesh {
                name = MeshName ?? "MeshGenerator",
                vertices = Vertices.ToArray(),
                uv = Uvs.ToArray(),
                triangles = Indices.ToArray()
            };

            body.RecalculateNormals();

            gameObject.GetComponent<MeshFilter>().sharedMesh = body;
        }

        internal void ClearMesh() {
            gameObject.GetComponent<MeshFilter>().sharedMesh.Clear();
        }

        internal abstract void ResetMeshData();

        internal abstract void RecalculateVertices();

        internal abstract void RecalculateIndices();

        internal void SewPolygonsBetweenEqualLoops(int points, int loops) {
            var indices = new List<int>();
            for (int l = 0; l < loops + 1; l++) {
                int offset = points*l;
                for (int p = 0; p < points - 1; p++) {
                    int i1 = p;
                    int i2 = (p + 1)%points;
                    int i3 = i1 + points;
                    int i4 = i2 + points;

                    indices.Add(i4 + offset);
                    indices.Add(i3 + offset);
                    indices.Add(i1 + offset);

                    indices.Add(i2 + offset);
                    indices.Add(i4 + offset);
                    indices.Add(i1 + offset);
                }
            }

            Indices = indices;
        }

        protected void OnPropertyChanged(string propertyName) {
            OnPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void OnPropertyChanged(Object sender, PropertyChangedEventArgs e) {
            RebuildMesh();
            if (PropertyChanged != null) {
                PropertyChanged(sender, e);
            }
        }

    }

}