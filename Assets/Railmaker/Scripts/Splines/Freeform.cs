using System;
using UnityEngine;

namespace Railmaker {

    [Serializable]
    [AddComponentMenu("Railmaker/Splines/Freeform")]
    public class Freeform : Spline {

        public override void SetPoints() {
        }

    }

}