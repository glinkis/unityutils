﻿using UnityEngine;

namespace MeshTools.Primitives
{
    public class Square
    {
        public readonly Mesh Mesh = new Mesh();

        public Square()
        {
            Build(0.5f, 0.5f);
        }

        public Square(float width, float height)
        {
            Build(width*0.5f, height*0.5f);
        }

        private void Build(float width, float height)
        {
            Mesh.vertices = new Vector3[]
            {
                new Vector3(-width, 0, height),
                new Vector3(width, 0, height),
                new Vector3(width, 0, -height),
                new Vector3(-width, 0, -height),
            };

            Mesh.uv = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(0, 1),
            };

            Mesh.triangles = new int[]
            {
                0, 1, 2,
                2, 3, 0,
            };

            Mesh.colors = new Color[]
            {
                new Color(width, 2/height, 1),
                new Color(width, 2/height, 1),
                new Color(width, 2/height, 1),
                new Color(width, 2/height, 1),
            };

            Mesh.RecalculateNormals();
        }
    }
}