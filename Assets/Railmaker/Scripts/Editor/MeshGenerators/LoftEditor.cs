﻿namespace Railmaker
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Loft))]
    public class LoftEditor : MeshGeneratorEditor
    {
        private Loft _loft;

        public void OnEnable()
        {
            MeshGenerator = (MeshGenerator)target;
            _loft = (Loft)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            TitleGui();
            AttributesGui();
        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Loft Controls", style);
        }

        private void AttributesGui()
        {
            Undo.RecordObject(_loft, "Loft Manipulation");
            _loft.SubdivisionsU = EditorGUILayout.IntField("Subdivisions U", _loft.SubdivisionsU);
            _loft.SubdivisionsV = EditorGUILayout.IntField("Subdivisions V", _loft.SubdivisionsV);
            _loft.SubdividePerSegment = EditorGUILayout.Toggle("Subdivide Segments", _loft.SubdividePerSegment);
            _loft.LinearInterpolation = EditorGUILayout.Toggle("Linear Interpolation", _loft.LinearInterpolation);
            Undo.ClearUndo(_loft);
        }

        #endregion Inspector
    }
}