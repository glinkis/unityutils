﻿using System.Collections.Generic;
using UnityEngine;

namespace MeshTools
{
    public static class MeshHelper
    {
        private static List<Vector3> vertices;
        private static List<Vector3> normals;
        private static List<Color> colors;
        private static List<Vector2> uv;
        private static List<Vector2> uv1;
        private static List<Vector2> uv2;

        private static List<int> indices;
        private static Dictionary<uint, int> newVectices;

        private static void InitArrays(Mesh mesh)
        {
            vertices = new List<Vector3>(mesh.vertices);
            normals = new List<Vector3>(mesh.normals);
            colors = new List<Color>(mesh.colors);
            uv = new List<Vector2>(mesh.uv);
            uv1 = new List<Vector2>(mesh.uv2);
            uv2 = new List<Vector2>(mesh.uv2);
            indices = new List<int>();
        }

        private static void CleanUp()
        {
            vertices = null;
            normals = null;
            colors = null;
            uv = null;
            uv1 = null;
            uv2 = null;
            indices = null;
        }

        public static void MoveMesh(Mesh mesh, Vector3 position)
        {
            Vector3[] movedVerts = new Vector3[mesh.vertices.Length];

            for (int i = 0; i < mesh.vertices.Length; i++)
                movedVerts[i] = mesh.vertices[i] + position;

            mesh.vertices = movedVerts;
        }

        public static void RotateMesh(Mesh mesh, Vector3 rotation)
        {
            Vector3[] rotatedVerts = new Vector3[mesh.vertices.Length];
            Quaternion qAngle = Quaternion.Euler(rotation);

            for (int i = 0; i < mesh.vertices.Length; i++)
                rotatedVerts[i] = qAngle * mesh.vertices[i];

            mesh.vertices = rotatedVerts;
        }

        public static Mesh DuplicateMesh(Mesh mesh)
        {
            return (Mesh)Object.Instantiate(mesh);
        }

        public static void Roundify(Mesh mesh)
        {
            Vector3[] baseVertices = mesh.vertices;
            Vector3[] vertices = new Vector3[baseVertices.Length];

            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3 vertex = baseVertices[i];
                vertex = vertex.normalized;
                vertices[i] = vertex;
            }
            mesh.vertices = vertices;
            mesh.RecalculateBounds();
        }

        public static void Combine(Mesh mesh, List<Mesh> meshes)
        {
            CombineInstance[] combine = new CombineInstance[meshes.Count];
            int i = 0;
            foreach (Mesh m in meshes)
            {
                combine[i].mesh = m;
                i++;
            }
            mesh.CombineMeshes(combine, true, false);
        }


        /// <summary> Averages all the vertex normals in a mesh. </summary>
        public static void AverageNormals(Mesh mesh)
        {
            Vector3[] verts = mesh.vertices;
            Vector3[] originalNormals = mesh.normals;
            Vector3[] recalculatedNormals = new Vector3[originalNormals.Length];

            for (int n1 = 0; n1 < verts.Length; n1++) // For each vertex
                for (int n2 = 0; n2 < verts.Length; n2++) // Look at each vertex
                    if (verts[n1] == verts[n2]) // If position is the same
                        recalculatedNormals[n1] = (originalNormals[n1] + originalNormals[n2]) * 0.5f; // Average Normals

            mesh.normals = recalculatedNormals;
        }

        #region Subdivide4 (2x2)

        private static int GetNewVertex4(int i1, int i2)
        {
            int newIndex = vertices.Count;
            uint t1 = ((uint)i1 << 16) | (uint)i2;
            uint t2 = ((uint)i2 << 16) | (uint)i1;
            if (newVectices.ContainsKey(t2))
                return newVectices[t2];
            if (newVectices.ContainsKey(t1))
                return newVectices[t1];

            newVectices.Add(t1, newIndex);

            vertices.Add((vertices[i1] + vertices[i2]) * 0.5f);
            if (normals.Count > 0)
                normals.Add((normals[i1] + normals[i2]).normalized);
            if (colors.Count > 0)
                colors.Add((colors[i1] + colors[i2]) * 0.5f);
            if (uv.Count > 0)
                uv.Add((uv[i1] + uv[i2]) * 0.5f);
            if (uv1.Count > 0)
                uv1.Add((uv1[i1] + uv1[i2]) * 0.5f);
            if (uv2.Count > 0)
                uv2.Add((uv2[i1] + uv2[i2]) * 0.5f);

            return newIndex;
        }

        private static void Subdivide4(Mesh mesh)
        {
            //newVectices = new Dictionary<uint, int>();

            InitArrays(mesh);

            int[] triangles = mesh.triangles;
            for (int i = 0; i < triangles.Length; i += 3)
            {
                int i1 = triangles[i + 0];
                int i2 = triangles[i + 1];
                int i3 = triangles[i + 2];

                int a = GetNewVertex4(i1, i2);
                int b = GetNewVertex4(i2, i3);
                int c = GetNewVertex4(i3, i1);

                indices.Add(i1);
                indices.Add(a);
                indices.Add(c);
                indices.Add(i2);
                indices.Add(b);
                indices.Add(a);
                indices.Add(i3);
                indices.Add(c);
                indices.Add(b);
                indices.Add(a);
                indices.Add(b);
                indices.Add(c); // center triangle
            }
            mesh.vertices = vertices.ToArray();
            if (normals.Count > 0)
                mesh.normals = normals.ToArray();
            if (colors.Count > 0)
                mesh.colors = colors.ToArray();
            if (uv.Count > 0)
                mesh.uv = uv.ToArray();
            if (uv1.Count > 0)
                mesh.uv2 = uv1.ToArray();
            if (uv2.Count > 0)
                mesh.uv2 = uv2.ToArray();

            mesh.triangles = indices.ToArray();

            CleanUp();
        }

        #endregion Subdivide4 (2x2)

        #region Subdivide9 (3x3)

        private static int GetNewVertex9(int i1, int i2, int i3)
        {
            int newIndex = vertices.Count;

            // center points don't go into the edge list
            if (i3 == i1 || i3 == i2)
            {
                uint t1 = ((uint)i1 << 16) | (uint)i2;
                if (newVectices.ContainsKey(t1))
                    return newVectices[t1];
                newVectices.Add(t1, newIndex);
            }

            // calculate new vertex
            vertices.Add((vertices[i1] + vertices[i2] + vertices[i3]) / 3.0f);
            if (normals.Count > 0)
                normals.Add((normals[i1] + normals[i2] + normals[i3]).normalized);
            if (colors.Count > 0)
                colors.Add((colors[i1] + colors[i2] + colors[i3]) / 3.0f);
            if (uv.Count > 0)
                uv.Add((uv[i1] + uv[i2] + uv[i3]) / 3.0f);
            if (uv1.Count > 0)
                uv1.Add((uv1[i1] + uv1[i2] + uv1[i3]) / 3.0f);
            if (uv2.Count > 0)
                uv2.Add((uv2[i1] + uv2[i2] + uv2[i3]) / 3.0f);
            return newIndex;
        }

        private static void Subdivide9(Mesh mesh)
        {
            newVectices = new Dictionary<uint, int>();

            InitArrays(mesh);

            int[] triangles = mesh.triangles;
            for (int i = 0; i < triangles.Length; i += 3)
            {
                int i1 = triangles[i + 0];
                int i2 = triangles[i + 1];
                int i3 = triangles[i + 2];

                int a1 = GetNewVertex9(i1, i2, i1);
                int a2 = GetNewVertex9(i2, i1, i2);
                int b1 = GetNewVertex9(i2, i3, i2);
                int b2 = GetNewVertex9(i3, i2, i3);
                int c1 = GetNewVertex9(i3, i1, i3);
                int c2 = GetNewVertex9(i1, i3, i1);

                int d = GetNewVertex9(i1, i2, i3);

                indices.Add(i1);
                indices.Add(a1);
                indices.Add(c2);
                indices.Add(i2);
                indices.Add(b1);
                indices.Add(a2);
                indices.Add(i3);
                indices.Add(c1);
                indices.Add(b2);
                indices.Add(d);
                indices.Add(a1);
                indices.Add(a2);
                indices.Add(d);
                indices.Add(b1);
                indices.Add(b2);
                indices.Add(d);
                indices.Add(c1);
                indices.Add(c2);
                indices.Add(d);
                indices.Add(c2);
                indices.Add(a1);
                indices.Add(d);
                indices.Add(a2);
                indices.Add(b1);
                indices.Add(d);
                indices.Add(b2);
                indices.Add(c1);
            }

            mesh.vertices = vertices.ToArray();
            if (normals.Count > 0)
                mesh.normals = normals.ToArray();
            if (colors.Count > 0)
                mesh.colors = colors.ToArray();
            if (uv.Count > 0)
                mesh.uv = uv.ToArray();
            if (uv1.Count > 0)
                mesh.uv2 = uv1.ToArray();
            if (uv2.Count > 0)
                mesh.uv2 = uv2.ToArray();

            mesh.triangles = indices.ToArray();

            CleanUp();
        }

        #endregion Subdivide9 (3x3)

        #region Subdivide

        public static void Subdivide(Mesh mesh, int level)
        {
            if (level < 2)
                return;
            while (level > 1)
            {
                // remove prime factor 3
                while (level % 3 == 0)
                {
                    Subdivide9(mesh);
                    level /= 3;
                }
                // remove prime factor 2
                while (level % 2 == 0)
                {
                    Subdivide4(mesh);
                    level /= 2;
                }
                // try to approximate. All other primes are increased by one
                // so they can be processed
                if (level > 3)
                    level++;
            }
        }

        #endregion Subdivide
    }
}