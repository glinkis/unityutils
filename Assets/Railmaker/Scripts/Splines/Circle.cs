using System;
using UnityEngine;

namespace Railmaker {

    [Serializable]
    [AddComponentMenu("Railmaker/Splines/Circle")]
    public class Circle : Spline {

        public enum SizeTypes {

            Radius,

            Diameter,

            Circumference,

            Area

        }

        [SerializeField] private float size;

        [SerializeField] private SizeTypes sizeType;

        private Circle() {
            Size = 0.5f;
            SizeType = SizeTypes.Radius;
            IntermediatePointsNumber = 6;
        }

        public float Size {
            get { return size; }
            set {
                size = value;
                OnPropertyChanged("Size");
            }
        }

        public SizeTypes SizeType {
            get { return sizeType; }
            set {
                if (sizeType == value) {
                    return;
                }
                sizeType = value;
                OnPropertyChanged("SizeType");
            }
        }

        public float Radius {
            get {
                switch (SizeType) {
                    case SizeTypes.Radius:
                        return Size;
                    case SizeTypes.Diameter:
                        return Size/2f;
                    case SizeTypes.Circumference:
                        return Size/(2f*Mathf.PI);
                    case SizeTypes.Area:
                        return Mathf.Sqrt(Size/Mathf.PI);
                    default:
                        throw new Exception("SizeType not defined.");
                }
            }
        }

        public override void SetPoints() {
            SetNumberOfPoints(4);
            SetPointPositions();
            SetTangentRelationships();
            SetTangentPositions();
        }

        private void SetPointPositions() {
            ControlPoints[0].Position = new Vector3(0, -Radius, 0);
            ControlPoints[1].Position = new Vector3(-Radius, 0, 0);
            ControlPoints[2].Position = new Vector3(0, Radius, 0);
            ControlPoints[3].Position = new Vector3(Radius, 0, 0);
        }

        private void SetTangentRelationships() {
            ControlPoints[0].TangentRelationship = Point.TangentRelationships.Symmetrical;
            ControlPoints[1].TangentRelationship = Point.TangentRelationships.Symmetrical;
            ControlPoints[2].TangentRelationship = Point.TangentRelationships.Symmetrical;
            ControlPoints[3].TangentRelationship = Point.TangentRelationships.Symmetrical;
        }

        private void SetTangentPositions() {
            ControlPoints[0].RightTangent = new Vector3(-Radius*0.5f, 0, 0);
            ControlPoints[1].RightTangent = new Vector3(0, Radius*0.5f, 0);
            ControlPoints[2].RightTangent = new Vector3(Radius*0.5f, 0, 0);
            ControlPoints[3].RightTangent = new Vector3(0, -Radius*0.5f, 0);
        }

    }

}