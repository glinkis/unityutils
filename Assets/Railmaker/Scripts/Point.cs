﻿using System;
using System.ComponentModel;
using UnityEngine;
using Object = System.Object;

namespace Railmaker {

    [Serializable]
    public class Point : INotifyPropertyChanged {

        public enum TangentRelationships {

            Broken,

            Locked,

            Symmetrical

        }

        [SerializeField] private Vector3 _leftTangent;

        [SerializeField] private Vector3 _position;

        [SerializeField] private Vector3 _rightTangent;

        [SerializeField] private TangentRelationships _tangentRelationship;

        public Point() {
            TangentRelationship = TangentRelationships.Locked;
            LeftTangent = new Vector3(0.25f, 0, 0);
            RightTangent = new Vector3(-0.25f, 0, 0);
        }

        public Vector3 Position {
            get { return _position; }
            set {
                if (value == _position) {
                    return;
                }
                _position = value;
                OnPropertyChanged("Position");
            }
        }

        public Vector3 LeftTangent {
            get { return _leftTangent; }
            set {
                if (value == _leftTangent) {
                    return;
                }
                _leftTangent = value;
                if (TangentRelationship == TangentRelationships.Locked) {
                    _rightTangent = InvertTangentRotation(_rightTangent, _leftTangent);
                }
                if (TangentRelationship == TangentRelationships.Symmetrical) {
                    _rightTangent = -_leftTangent;
                }
                OnPropertyChanged("LeftTangent");
            }
        }

        public Vector3 LeftTangentAbsolute {
            get { return LeftTangent + Position; }
            set { LeftTangent = value - Position; }
        }

        public Vector3 RightTangent {
            get { return _rightTangent; }
            set {
                if (value == _rightTangent) {
                    return;
                }
                _rightTangent = value;
                if (TangentRelationship == TangentRelationships.Locked) {
                    _leftTangent = InvertTangentRotation(_leftTangent, _rightTangent);
                }
                if (TangentRelationship == TangentRelationships.Symmetrical) {
                    _leftTangent = -_rightTangent;
                }
                OnPropertyChanged("RightTangent");
            }
        }

        public Vector3 RightTangentAbsolute {
            get { return RightTangent + Position; }
            set { RightTangent = value - Position; }
        }

        public TangentRelationships TangentRelationship {
            get { return _tangentRelationship; }
            set {
                if (value == _tangentRelationship) {
                    return;
                }
                _tangentRelationship = value;
                OnPropertyChanged("TangentRelationship");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private Vector3 InvertTangentRotation(Vector3 a, Vector3 b) {
            return -b.normalized*a.magnitude;
        }

        private void OnPropertyChanged(string propertyName) {
            OnPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnPropertyChanged(Object sender, PropertyChangedEventArgs e) {
            if (PropertyChanged != null) {
                PropertyChanged(sender, e);
            }
        }

    }

}