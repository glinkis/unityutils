﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Railmaker {

    [Serializable]
    [AddComponentMenu("Railmaker/SplineMeshes/Sweep")]
    [RequireComponent(typeof (MeshFilter))]
    [RequireComponent(typeof (MeshRenderer))]
    public class Sweep : MeshGenerator {

        [SerializeField] private float _endGrowth;

        [SerializeField] private float _endScale;

        private List<Vector3> _splinePointsU;

        private List<Vector3> _splinePointsV;

        private Vector3 _splinePosition;

        [SerializeField] private Spline _splineU;

        [SerializeField] private Spline _splineV;

        [SerializeField] private float _startGrowth;

        private Quaternion loopRotation;

        public Spline SplineU {
            get { return _splineU; }
            set {
                if (_splineU == value) {
                    return;
                }
                if (_splineU != null) {
                    _splineU.PropertyChanged -= OnPropertyChanged;
                }
                if (value != null) {
                    value.PropertyChanged += OnPropertyChanged;
                }
                _splineU = value;
                OnPropertyChanged("SplineU");
            }
        }

        public Spline SplineV {
            get { return _splineV; }
            set {
                if (_splineV == value) {
                    return;
                }
                if (_splineV != null) {
                    _splineV.PropertyChanged -= OnPropertyChanged;
                }
                if (value != null) {
                    value.PropertyChanged += OnPropertyChanged;
                }
                _splineV = value;
                OnPropertyChanged("SplineV");
            }
        }

        public float EndScale {
            get { return _endScale; }
            set {
                _endScale = value;
                OnPropertyChanged("EndScale");
            }
        }

        public float StartGrowth {
            get { return _startGrowth; }
            set {
                _startGrowth = value;
                OnPropertyChanged("StartGrowth");
            }
        }

        public float EndGrowth {
            get { return _endGrowth; }
            set {
                _endGrowth = value;
                OnPropertyChanged("EndGrowth");
            }
        }

        public override void Awake() {
            MeshName = "Sweep";
            EndScale = 100f;
            StartGrowth = 0f;
            EndGrowth = 100f;
        }

        internal override void ResetMeshData() {
            if (SplineU == null || SplineV == null) {
                return;
            }
            _splinePosition = SplineV.transform.localPosition;
            _splinePointsU = SplineU.GetInterpolatedPoints();
            _splinePointsV = SplineV.GetInterpolatedPoints();
            Vertices = new List<Vector3>();
            Uvs = new List<Vector2>();
        }

        internal override void RecalculateVertices() {
            if (SplineU == null || SplineV == null) {
                return;
            }
            for (int vPt = 0; vPt < _splinePointsV.Count; vPt++) {
                SetLoopRotation(vPt);
                GoThroughPoints(vPt);
            }
        }

        internal override void RecalculateIndices() {
            if (SplineU == null || SplineV == null) {
                return;
            }
            SewPolygonsBetweenEqualLoops(_splinePointsU.Count, _splinePointsV.Count - 2);
        }

        private void SetLoopRotation(int vPt) {
            if (vPt != _splinePointsV.Count - 1) {
                loopRotation = Quaternion.LookRotation(_splinePointsV[vPt + 1] - _splinePointsV[vPt]);
            }
            else {
                loopRotation = Quaternion.LookRotation(-(_splinePointsV[vPt - 1] - _splinePointsV[vPt]));
            }
        }

        private void GoThroughPoints(int vPt) {
            for (int uPt = 0; uPt < _splinePointsU.Count; uPt++) {
                Vertices.Add(GetVertexPosition(vPt, uPt));
                Uvs.Add(GetUvPosition(uPt, vPt));
            }
        }

        private Vector3 GetVertexPosition(int vPt, int uPt) {
            float step = (1f/_splinePointsV.Count)*vPt;
            float unit = 1 - (0.01f*EndScale);
            Vector3 scale = Vector3.Lerp(_splinePointsU[uPt], Vector3.zero, step*unit);
            return _splinePosition + (loopRotation*scale) + _splinePointsV[vPt];
        }

        private Vector2 GetUvPosition(int uPt, int vPt) {
            float u = (1f/(_splinePointsU.Count - 1))*uPt;
            float v = (1f/(_splinePointsV.Count + 1))*vPt;
            return new Vector2(u, v);
        }

    }

}