﻿namespace Railmaker
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Lathe))]
    public class LatheEditor : MeshGeneratorEditor
    {
        private Lathe _lathe;

        public void OnEnable()
        {
            MeshGenerator = (MeshGenerator)target;
            _lathe = (Lathe)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            TitleGui();
            AttributesGui();
        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Lathe Controls", style);
        }

        private void AttributesGui()
        {
            Undo.RecordObject(_lathe, "Lathe Manipulation");
            _lathe.Spline = (Spline)EditorGUILayout.ObjectField("Spline", _lathe.Spline, typeof(Spline), true);
            _lathe.Subdivisions = EditorGUILayout.IntField("Subdivisions", _lathe.Subdivisions);
            _lathe.Angle = EditorGUILayout.FloatField("Angle", _lathe.Angle);
            _lathe.Movement = EditorGUILayout.FloatField("Movement", _lathe.Movement);
            Undo.ClearUndo(_lathe);
        }

        #endregion Inspector
    }
}