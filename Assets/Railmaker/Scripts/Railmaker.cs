﻿using System;
using UnityEditor;
using UnityEngine;

namespace Railmaker {

    public static class Railmaker {

        [MenuItem("GameObject/Railmaker/Splines/Freeform", false, 1)]
        public static Freeform CreateFreeform() {
            return (Freeform) NewRailmakerObject<Freeform>();
        }

        [MenuItem("GameObject/Railmaker/Splines/Rectangle", false, 1)]
        public static Rectangle CreateRectangle() {
            return (Rectangle) NewRailmakerObject<Rectangle>();
        }

        [MenuItem("GameObject/Railmaker/Splines/Circle", false, 1)]
        public static Circle CreateCircle() {
            return (Circle) NewRailmakerObject<Circle>();
        }

        [MenuItem("GameObject/Railmaker/Meshes/Loft", false, 1)]
        public static Loft CreateLoft() {
            return (Loft) NewRailmakerObject<Loft>();
        }

        [MenuItem("GameObject/Railmaker/Meshes/Extrude", false, 1)]
        public static Extrude CreateExtrude() {
            return (Extrude) NewRailmakerObject<Extrude>();
        }

        [MenuItem("GameObject/Railmaker/Meshes/Lathe", false, 1)]
        public static Lathe CreateLathe() {
            return (Lathe) NewRailmakerObject<Lathe>();
        }

        [MenuItem("GameObject/Railmaker/Meshes/Sweep", false, 1)]
        public static Sweep CreateSweep() {
            return (Sweep) NewRailmakerObject<Sweep>();
        }

        private static Component NewRailmakerObject<T>() {
            Type type = typeof (T);
            GameObject gameObject = new GameObject(type.Name);
            Component obj = gameObject.AddComponent(type);
            Selection.activeObject = gameObject;
            return obj;
        }

    }

}