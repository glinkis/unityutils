﻿using System.Collections.Generic;
using MeshTools;
using MeshTools.Primitives;
using UnityEngine;

namespace AutoSubdSphere
{
    public class Chunk
    {
        public readonly int Depth;

        private readonly Vector2 position = new Vector2();
        private readonly Vector3 centerOffset = new Vector3(0, 0.5f, 0);
        private readonly Vector3 rotation;
        private readonly float size;

        public List<Chunk> Children;
        public Mesh Mesh;

        public Chunk(Vector3 rot)
        {
            Depth = 0;

            centerOffset = new Vector3(0, 0.5f, 0);
            rotation = rot;
            size = 1f;

            Children = new List<Chunk>();
            Mesh = new Square().Mesh;

            MeshHelper.MoveMesh(Mesh, centerOffset);
            MeshHelper.RotateMesh(Mesh, rotation);
        }

        private Chunk(Vector2 relPos, Vector3 rot, int depth, Vector2 position, float size)
        {
            Depth = depth + 1;

            size = size*0.5f;
            position = (relPos*(size*0.5f)) + position;
            rotation = rot;

            Children = new List<Chunk>();
            Mesh = new Square(size, size).Mesh;

            MeshHelper.MoveMesh(Mesh, new Vector3(position.x, centerOffset.y, position.y));
            MeshHelper.RotateMesh(Mesh, rotation);
        }

        public void UpRes()
        {
            Mesh = null;
            Children = new List<Chunk>
            {
                new Chunk(new Vector2(-1f, -1f), rotation, Depth, position, size),
                new Chunk(new Vector2(-1f, 1f), rotation, Depth, position, size),
                new Chunk(new Vector2(1f, -1f), rotation, Depth, position, size),
                new Chunk(new Vector2(1f, 1f), rotation, Depth, position, size),
            };
        }

        public void DownRes()
        {
            Children = new List<Chunk>();
            Mesh = new Square().Mesh;

            MeshHelper.MoveMesh(Mesh, centerOffset);
            MeshHelper.RotateMesh(Mesh, rotation);
        }
    }
}