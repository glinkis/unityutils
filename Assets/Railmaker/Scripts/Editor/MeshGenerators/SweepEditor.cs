﻿namespace Railmaker
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Sweep), false)]
    public class SweepEditor : MeshGeneratorEditor
    {
        private Sweep _sweep;

        public void OnEnable()
        {
            MeshGenerator = (MeshGenerator)target;
            _sweep = (Sweep)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            TitleGui();
            AttributesGui();
        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Sweep Controls", style);
        }

        private void AttributesGui()
        {
            Undo.RecordObject(_sweep, "Sweep Manipulation");
            _sweep.SplineU = (Spline)EditorGUILayout.ObjectField("Spline U", _sweep.SplineU, typeof(Spline), true);
            _sweep.SplineV = (Spline)EditorGUILayout.ObjectField("Spline V", _sweep.SplineV, typeof(Spline), true);
            _sweep.EndScale = EditorGUILayout.FloatField("End Scale", _sweep.EndScale);
            //// _sweep.StartGrowth = EditorGUILayout.FloatField("Start Growth", _sweep.StartGrowth);
            //// _sweep.EndGrowth = EditorGUILayout.FloatField("End Growth", _sweep.EndGrowth);
            Undo.ClearUndo(_sweep);
        }

        #endregion Inspector
    }
}