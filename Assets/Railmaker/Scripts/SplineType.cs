﻿using System.Collections.Generic;
using UnityEngine;

namespace Railmaker {

    public abstract class SplineType {

        /// <summary>
        /// Represents the number of required points for the current interpolation.
        /// </summary>
        public abstract int RequiredPoints { get; }

        public abstract Vector3[] InterpolatePoints(IList<Point> points, float k);

        /// <summary>
        /// Does a linear interpolation from 2 control points.
        /// </summary>
        public sealed class Linear : SplineType {

            public override int RequiredPoints {
                get { return 2; }
            }

            public override Vector3[] InterpolatePoints(IList<Point> points, float k) {
                Vector3[] values = {
                    points[0].Position,
                    points[1].Position
                };

                return new Vector3[] {
                    new InterpolationMethod.Linear().CalculateInterpolation(k, values),
                    points[1].Position
                };
            }

        }

        /// <summary>
        /// Does a cubic interpolation from 2 control points, using the tangents.
        /// </summary>
        public sealed class Bezier : SplineType {

            public override int RequiredPoints {
                get { return 2; }
            }

            public override Vector3[] InterpolatePoints(IList<Point> points, float k) {
                Vector3[] values = {
                    points[0].Position,
                    points[0].RightTangentAbsolute,
                    points[1].LeftTangentAbsolute,
                    points[1].Position
                };

                return new Vector3[] {
                    new InterpolationMethod.Cubic().CalculateInterpolation(k, values),
                    points[1].Position
                };
            }

        }

        /// <summary>
        /// Does a cubic interpolation betweem 4 points.
        /// </summary>
        public sealed class Cubic : SplineType {

            public override int RequiredPoints {
                get { return 4; }
            }

            public override Vector3[] InterpolatePoints(IList<Point> points, float k) {
                Vector3[] values = {
                    points[0].Position,
                    points[1].Position,
                    points[2].Position,
                    points[3].Position
                };

                return new Vector3[] {
                    new InterpolationMethod.Cubic().CalculateInterpolation(k, values),
                    points[2].Position
                };
            }

        }

    }

}