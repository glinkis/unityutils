﻿using UnityEditor;
using UnityEngine;

namespace Railmaker {

    [CustomEditor(typeof (Freeform))]
    [CanEditMultipleObjects]
    public class FreeformEditor : SplineEditor {

        [SerializeField] private Freeform _freeform;

        private bool pointsGui;

        public new void OnEnable() {
            base.OnEnable();
            _freeform = (Freeform) Spline;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            TitleGui();
            AddPointGui();
            DrawPointsGui();
        }

        #region Inspector

        private void TitleGui() {
            GUIStyle style = new GUIStyle {fontStyle = FontStyle.Bold};
            EditorGUILayout.LabelField("Freeform Controls", style);
        }

        private void AddPointGui() {
            EditorGUILayout.BeginHorizontal();
            AddPointAtStartGui();
            AddPointAtEndGui();
            EditorGUILayout.EndHorizontal();
        }

        private void AddPointAtStartGui() {
            if (GUILayout.Button("Point+ (Start)")) {
                _freeform.ControlPoints.Insert(0, new Point());
            }
        }

        private void AddPointAtEndGui() {
            if (GUILayout.Button("Point+ (End)")) {
                _freeform.ControlPoints.Add(new Point());
            }
        }

        private void DrawPointsGui() {
            pointsGui = EditorGUILayout.Foldout(pointsGui, "Points");
            if (!pointsGui) {
                return;
            }
            for (int i = 0; i < Spline.ControlPoints.Count; i++) {
                PointTitleGui(i);
                PointPoisitionGui(i);
                PointTangentsGui(i);
                PointActionsGui(i);
                EditorGUILayout.Space();
            }
        }

        private void PointPoisitionGui(int i) {
            Spline.ControlPoints[i].Position = EditorGUILayout.Vector3Field("Position", Spline.ControlPoints[i].Position);
        }

        private void PointTangentsGui(int i) {
            Spline.ControlPoints[i].TangentRelationship = (Point.TangentRelationships)
                EditorGUILayout.EnumPopup("Tangent Relationship", Spline.ControlPoints[i].TangentRelationship);
            Spline.ControlPoints[i].RightTangent =
                EditorGUILayout.Vector3Field("RightTangent", Spline.ControlPoints[i].RightTangent);
            Spline.ControlPoints[i].LeftTangent =
                EditorGUILayout.Vector3Field("LeftTangent", Spline.ControlPoints[i].LeftTangent);
        }

        private void PointTitleGui(int i) {
            GUIStyle style = new GUIStyle {fontStyle = FontStyle.Bold};
            EditorGUILayout.LabelField("point" + i, style);
        }

        private void PointActionsGui(int i) {
            EditorGUILayout.BeginHorizontal();
            RemovePointGui(i);
            MovePointUp(i);
            MovePointDown(i);
            EditorGUILayout.EndHorizontal();
        }

        private void RemovePointGui(int i) {
            if (GUILayout.Button("Remove", GUILayout.Width(80))) {
                _freeform.ControlPoints.Remove(_freeform.ControlPoints[i]);
            }
        }

        private void MovePointUp(int i) {
            if (!GUILayout.Button("↑", GUILayout.Width(20))) {
                return;
            }
            if (i <= 0) {
                return;
            }
            Point p = _freeform.ControlPoints[i];
            _freeform.ControlPoints.RemoveAt(i);
            _freeform.ControlPoints.Insert(i - 1, p);
        }

        private void MovePointDown(int i) {
            if (!GUILayout.Button("↓", GUILayout.Width(20))) {
                return;
            }
            if (i >= _freeform.ControlPoints.Count - 1) {
                return;
            }
            Point p = _freeform.ControlPoints[i];
            _freeform.ControlPoints.RemoveAt(i);
            _freeform.ControlPoints.Insert(i + 1, p);
        }

        #endregion Inspector
    }

}