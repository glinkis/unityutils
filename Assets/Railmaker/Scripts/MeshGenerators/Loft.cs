﻿using System;
using UnityEngine;

namespace Railmaker {

    [Serializable]
    [AddComponentMenu("Railmaker/SplineMeshes/Loft")]
    [RequireComponent(typeof (MeshFilter))]
    [RequireComponent(typeof (MeshRenderer))]
    public class Loft : MeshGenerator {

        [SerializeField] private bool _linearInterpolation;

        [SerializeField] private bool _subdividePerSegment;

        [SerializeField] private int _subdivisionsU;

        [SerializeField] private int _subdivisionsV;

        public bool LinearInterpolation { get; set; }

        public bool SubdividePerSegment { get; set; }

        public int SubdivisionsU { get; set; }

        public int SubdivisionsV { get; set; }

        public override void Awake() {
            MeshName = "Loft";
            LinearInterpolation = false;
            SubdividePerSegment = false;
            SubdivisionsU = 6;
            SubdivisionsV = 6;
        }

        internal override void ResetMeshData() {
            throw new NotImplementedException();
        }

        internal override void RecalculateVertices() {
            throw new NotImplementedException();
        }

        internal override void RecalculateIndices() {
            throw new NotImplementedException();
        }

    }

}