namespace Railmaker
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Circle))]
    [CanEditMultipleObjects]
    public class CircleEditor : SplineEditor
    {
        private Circle _circle;

        public new void OnEnable()
        {
            base.OnEnable();
            _circle = (Circle)Spline;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            TitleGui();
            AttributesGui();
        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Circle Controls", style);
        }

        private void AttributesGui()
        {
            Undo.RecordObject(_circle, "Circle Manipulation");
            _circle.Size = EditorGUILayout.FloatField("Size", _circle.Size);
            _circle.SizeType = (Circle.SizeTypes)EditorGUILayout.EnumPopup("Size Represents", _circle.SizeType);
            _circle.SetPoints();
            Undo.ClearUndo(_circle);
        }

        #endregion Inspector
    }
}