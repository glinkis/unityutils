﻿using System.Collections.Generic;
using System.Linq;
using MeshTools;
using UnityEngine;

namespace AutoSubdSphere
{
    public class ChunkBall
    {
        public readonly List<Chunk> Chunks;
        public readonly Mesh Mesh;

        private int subdivisionLevel;

        public ChunkBall()
        {
            Mesh = new Mesh();
            Chunks = new List<Chunk>
            {
                new Chunk(new Vector3(0, 0, 0)),
                new Chunk(new Vector3(180, 0, 0)),
                new Chunk(new Vector3(0, 0, 90)),
                new Chunk(new Vector3(0, 0, -90)),
                new Chunk(new Vector3(90, 0, 0)),
                new Chunk(new Vector3(0, 270, 90)),
            };

            BuildMesh();
        }

        private void BuildMesh()
        {
            MeshHelper.Combine(Mesh, GetDeepestChunkMeshes());
            MeshHelper.Subdivide(Mesh, 4);
            MeshHelper.Roundify(Mesh);

            Mesh.Optimize();
            Mesh.RecalculateNormals();
        }

        public void UpResAll()
        {
            if (subdivisionLevel == 3) return;
            subdivisionLevel++;

            foreach (Chunk chunk in GetAllChunks().Where(chunk => chunk.Children.Count == 0))
                chunk.UpRes();

            BuildMesh();
            //Debug.Log("Upressed to lvl " + subdivisionLevel + ". " + GetAllChunks().Count() + " total chunks.");
        }

        public void DownResAll()
        {
            if (subdivisionLevel == 0) return;
            subdivisionLevel--;

            foreach (Chunk chunk in GetDeepestChunkGroups())
                chunk.DownRes();

            BuildMesh();
            //Debug.Log("Downressed to lvl " + subdivisionLevel + ". " + GetAllChunks().Count() + " total chunks.");
        }

        private static void AddAllChunksRecursive(List<Chunk> list, Chunk parent)
        {
            foreach (Chunk ch in parent.Children)
                AddAllChunksRecursive(list, ch);

            list.AddRange(parent.Children);
        }

        private IEnumerable<Chunk> GetAllChunks()
        {
            List<Chunk> res = new List<Chunk>();

            res.AddRange(Chunks);
            foreach (Chunk ch in Chunks)
                AddAllChunksRecursive(res, ch);

            return res;
        }

        private int GetChunkDepth()
        {
            int depth = 0;
            foreach (Chunk chunk in GetAllChunks())
                if (chunk.Depth > depth)
                    depth = chunk.Depth;

            return depth;
        }

        private IEnumerable<Chunk> GetDeepestChunks()
        {
            return GetAllChunks().Where(chunk => chunk.Depth == GetChunkDepth()).ToList();
        }

        private List<Mesh> GetAllChunkMeshes()
        {
            return GetAllChunks().Where(chunk => chunk.Children.Count == 0).Select(chunk => chunk.Mesh).ToList();
        }

        private List<Mesh> GetDeepestChunkMeshes()
        {
            return GetDeepestChunks().Where(chunk => chunk.Children.Count == 0).Select(chunk => chunk.Mesh).ToList();
        }

        private IEnumerable<Chunk> GetDeepestChunkGroups()
        {
            return
                GetAllChunks().Where(chunk => chunk.Depth == GetChunkDepth() - 1 && chunk.Children.Count == 4).ToList();
        }
    }
}