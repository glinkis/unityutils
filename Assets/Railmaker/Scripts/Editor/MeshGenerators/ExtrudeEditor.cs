﻿namespace Railmaker
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Extrude))]
    public class ExtrudeEditor : MeshGeneratorEditor
    {
        private Extrude _extrude;

        public void OnEnable()
        {
            MeshGenerator = (MeshGenerator)target;
            _extrude = (Extrude)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            TitleGui();
            AttributesGui();
        }

        public override void OnSceneGUI()
        {
            base.OnSceneGUI();
            LocalizeHandles();
            SceneManipulation();
        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Extrude Controls", style);
        }

        private void AttributesGui()
        {
            Undo.RecordObject(_extrude, "Extrude Manipulation");
            _extrude.Spline = (Spline)EditorGUILayout.ObjectField("Spline", _extrude.Spline, typeof(Spline), true);
            _extrude.Movement = EditorGUILayout.Vector3Field("Movement", _extrude.Movement);
            _extrude.Subdivisions = EditorGUILayout.IntField("Subdivisions", _extrude.Subdivisions);
            Undo.ClearUndo(_extrude);
        }

        #endregion Inspector

        #region Scene

        private void LocalizeHandles()
        {
            Handles.matrix = _extrude.transform.localToWorldMatrix;
        }

        private void SceneManipulation()
        {
            Undo.RecordObject(_extrude, "Extrude Manipulation");
            float size = HandleUtility.GetHandleSize(_extrude.Movement) * 0.15f;
            _extrude.Movement = Handles.FreeMoveHandle(_extrude.Movement, Quaternion.identity, size, Vector3.zero, Handles.CircleCap);
            Undo.ClearUndo(_extrude);
        }

        #endregion Scene
    }
}