using System;
using UnityEngine;

namespace Railmaker {

    [Serializable]
    [AddComponentMenu("Railmaker/Splines/Rectangle")]
    public class Rectangle : Spline {

        [SerializeField] private float height;

        [SerializeField] private float width;

        private Rectangle() {
            SplineType = new SplineType.Linear();
            Width = 1f;
            Height = 1f;
        }

        public float Height {
            get { return height; }
            set {
                height = value;
                OnPropertyChanged("Height");
            }
        }

        public float Width {
            get { return width; }
            set {
                width = value;
                OnPropertyChanged("Width");
            }
        }

        public override void SetPoints() {
            SetNumberOfPoints(4);
            SetPointPositions();
            SetTangentRelationships();
            SetTangentPositions();
        }

        private void SetPointPositions() {
            ControlPoints[0].Position = new Vector3(Width/2f, Height/2f, 0);
            ControlPoints[1].Position = new Vector3(-Width/2f, Height/2f, 0);
            ControlPoints[2].Position = new Vector3(-Width/2f, -Height/2f, 0);
            ControlPoints[3].Position = new Vector3(Width/2f, -Height/2f, 0);
        }

        private void SetTangentRelationships() {
            ControlPoints[0].TangentRelationship = Point.TangentRelationships.Symmetrical;
            ControlPoints[1].TangentRelationship = Point.TangentRelationships.Symmetrical;
            ControlPoints[2].TangentRelationship = Point.TangentRelationships.Symmetrical;
            ControlPoints[3].TangentRelationship = Point.TangentRelationships.Symmetrical;
        }

        private void SetTangentPositions() {
            ControlPoints[0].RightTangent = Vector3.zero;
            ControlPoints[1].RightTangent = Vector3.zero;
            ControlPoints[2].RightTangent = Vector3.zero;
            ControlPoints[3].RightTangent = Vector3.zero;
        }

    }

}