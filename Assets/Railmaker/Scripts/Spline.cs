using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using Object = System.Object;

namespace Railmaker {

    [Serializable]
    public abstract class Spline : MonoBehaviour, INotifyPropertyChanged {

        [SerializeField] private PointList _controlPoints;

        [SerializeField] private int _intermediatePointsNumber;

        [SerializeField] private List<Vector3> _interpolatedPoints;

        [SerializeField] private bool _isClosed;

        [SerializeField] private SplineType _splineType;

        protected Spline() {
            IsClosed = true;
            SplineType = new SplineType.Bezier();
            ControlPoints = new PointList();
        }

        /// <summary>
        /// Controls if the spline should be considered closed or not.
        /// </summary>
        public bool IsClosed {
            get { return _isClosed; }
            set {
                if (_isClosed == value) {
                    return;
                }
                _isClosed = value;
                OnPropertyChanged("IsClosed");
            }
        }

        public int IntermediatePointsNumber {
            get { return _intermediatePointsNumber; }
            set {
                if (_intermediatePointsNumber == value) {
                    return;
                }
                _intermediatePointsNumber = value;
                OnPropertyChanged("IntermediatePointsNumber");
            }
        }

        public SplineType SplineType {
            get { return _splineType; }
            set {
                if (_splineType == value) {
                    return;
                }
                _splineType = value;
                OnPropertyChanged("SplineType");
            }
        }

        public PointList ControlPoints {
            get { return _controlPoints; }
            set {
                if (_controlPoints == value) {
                    return;
                }
                value.PropertyChanged += OnPropertyChanged;
                _controlPoints = value;
                OnPropertyChanged("ControlPoints");
            }
        }

        public List<Vector3> InterpolatedPoints {
            get { return _interpolatedPoints; }
            set { _interpolatedPoints = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Awake() {
            SetPoints();
        }

        public abstract void SetPoints();

        public List<Vector3> GetInterpolatedPoints() {
            if (ControlPoints.Count == 0) {
                return new List<Vector3>();
            }

            var tmpPoints = ControlPoints.ToList();

            if (IsClosed) {
                tmpPoints.Add(ControlPoints.First());
            }

            var interpolatedPoints = new List<Vector3>();

            for (int i = 0; i < tmpPoints.Count - 1; i++) {
                var pointsToInterpolate = GetOffsetListItems(SplineType.RequiredPoints, tmpPoints, i);
                for (int j = 0; j < IntermediatePointsNumber + 2; j++) {
                    float k = 1f;
                    k /= IntermediatePointsNumber + 1;
                    k *= j;

                    Vector3 pt = SplineType.InterpolatePoints(pointsToInterpolate, k)[0];
                    Vector3 endPt = SplineType.InterpolatePoints(pointsToInterpolate, k)[1];

                    if ((pt != endPt) ||
                        (((i + 1)*(IntermediatePointsNumber + 1)) + j == (IntermediatePointsNumber + 1)*tmpPoints.Count)) {
                        interpolatedPoints.Add(pt);
                    }
                }
            }

            interpolatedPoints.Reverse();
            return interpolatedPoints;
        }

        internal void SetNumberOfPoints(int targetNumber) {
            for (int i = 0; i < Math.Abs(ControlPoints.Count - targetNumber); ++i) {
                if (ControlPoints.Count > targetNumber) {
                    ControlPoints.Remove(ControlPoints.Last());
                }
                else {
                    ControlPoints.Add(new Point());
                }
            }
        }

        private IList<T> GetOffsetListItems<T>(int numberOfItems, IList<T> list, int index) {
            IList<T> items = new T[numberOfItems];

            for (int j = 0; j < numberOfItems; ++j) {
                if (index + j >= list.Count && IsClosed) {
                    items[j] = GetOffsetListItem(list, index + j + 1);
                }
                else if (index + j < 0 && IsClosed) {
                    items[j] = GetOffsetListItem(list, index + j - 1);
                }
                else {
                    items[j] = GetOffsetListItem(list, index + j);
                }
            }

            return items;
        }

        private T GetOffsetListItem<T>(IList<T> list, int index) {
            return index < 0 ? list[(index%list.Count) + list.Count] : list[index%list.Count];
        }

        protected void OnPropertyChanged(string propertyName) {
            OnPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void OnPropertyChanged(Object sender, PropertyChangedEventArgs e) {
            if (PropertyChanged != null) {
                PropertyChanged(sender, e);
            }
        }

    }

}