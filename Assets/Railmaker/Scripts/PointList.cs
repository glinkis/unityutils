using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using Object = System.Object;

namespace Railmaker {

    [Serializable]
    public class PointList : IList<Point>, INotifyPropertyChanged {

        [SerializeField] private List<Point> _points;

        public PointList() {
            _points = new List<Point>();
        }

        public int Count {
            get { return _points.Count; }
        }

        public bool IsReadOnly { get; private set; }

        public Point this[int index] {
            get { return _points[index]; }
            set {
                _points[index] = value;
                OnPropertyChanged("");
            }
        }

        public IEnumerator<Point> GetEnumerator() {
            return _points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return _points.GetEnumerator();
        }

        public void Add(Point item) {
            item.PropertyChanged += OnPropertyChanged;
            _points.Add(item);
            OnPropertyChanged("");
        }

        public void Clear() {
            _points.Clear();
            foreach (Point p in _points) {
                p.PropertyChanged -= OnPropertyChanged;
            }
            OnPropertyChanged("");
        }

        public bool Contains(Point item) {
            return _points.Contains(item);
        }

        public void CopyTo(Point[] array, int arrayIndex) {
            _points.CopyTo(array, arrayIndex);
        }

        public bool Remove(Point item) {
            bool remove = _points.Remove(item);
            item.PropertyChanged -= OnPropertyChanged;
            OnPropertyChanged("");
            return remove;
        }

        public int IndexOf(Point item) {
            return _points.IndexOf(item);
        }

        public void Insert(int index, Point item) {
            item.PropertyChanged += OnPropertyChanged;
            _points.Insert(index, item);
            OnPropertyChanged("");
        }

        public void RemoveAt(int index) {
            _points[index].PropertyChanged -= OnPropertyChanged;
            _points.RemoveAt(index);
            OnPropertyChanged("");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName) {
            OnPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnPropertyChanged(Object sender, PropertyChangedEventArgs e) {
            if (PropertyChanged != null) {
                PropertyChanged(sender, e);
            }
        }

    }

}