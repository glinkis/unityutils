﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Railmaker {

    [Serializable]
    [AddComponentMenu("Railmaker/SplineMeshes/Extrude")]
    [RequireComponent(typeof (MeshFilter))]
    [RequireComponent(typeof (MeshRenderer))]
    public class Extrude : MeshGenerator {

        private Vector3 _loopPosition;

        [SerializeField] private Vector3 _movement;

        [SerializeField] private Spline _spline;

        private List<Vector3> _splinePoints;

        private Vector3 _splinePosition;

        [SerializeField] private int _subdivisions;

        public Spline Spline {
            get { return _spline; }

            set {
                if (_spline == value) {
                    return;
                }
                if (_spline != null) {
                    _spline.PropertyChanged -= OnPropertyChanged;
                }
                if (value != null) {
                    value.PropertyChanged += OnPropertyChanged;
                }
                _spline = value;
                OnPropertyChanged("Spline");
            }
        }

        public Vector3 Movement {
            get { return _movement; }
            set {
                _movement = value;
                OnPropertyChanged("Movement");
            }
        }

        public int Subdivisions {
            get { return _subdivisions; }
            set {
                _subdivisions = value;
                OnPropertyChanged("Subdivisions");
            }
        }

        public override void Awake() {
            MeshName = "Extrude";
            Movement = new Vector3(0, 0, 1);
            Subdivisions = 0;
        }

        internal override void ResetMeshData() {
            if (_spline == null) {
                return;
            }

            _splinePosition = Spline.transform.localPosition;
            _splinePoints = Spline.GetInterpolatedPoints();
            PointCount = _splinePoints.Count;
            Vertices = new List<Vector3>();
            Uvs = new List<Vector2>();
        }

        internal override void RecalculateVertices() {
            for (int loop = 0; loop <= Subdivisions + 1; loop++) {
                GoThroughSplinePoints(loop);
            }
        }

        internal override void RecalculateIndices() {
            SewPolygonsBetweenEqualLoops(PointCount, Subdivisions);
        }

        private void GoThroughSplinePoints(int loop) {
            _loopPosition = (Movement/(Subdivisions + 1))*loop;
            for (int pt = 0; pt < PointCount; pt++) {
                Vertices.Add(GetVertexPosition(pt));
                Uvs.Add(GetUvPosition(pt, loop));
            }
        }

        private Vector3 GetVertexPosition(int pt) {
            return _splinePosition + _loopPosition + _splinePoints[pt];
        }

        private Vector2 GetUvPosition(int pt, int loop) {
            float u = (1f/(PointCount - 1))*pt;
            float v = (1f/(Subdivisions + 1))*loop;
            return new Vector2(u, v);
        }

    }

}