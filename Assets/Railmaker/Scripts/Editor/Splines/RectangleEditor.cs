namespace Railmaker
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Rectangle))]
    [CanEditMultipleObjects]
    public class RectangleEditor : SplineEditor
    {
        private Rectangle _rectangle;

        public new void OnEnable()
        {
            base.OnEnable();
            _rectangle = (Rectangle)Spline;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            TitleGui();
            AttributesGui();
        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Rectangle Controls", style);
        }

        private void AttributesGui()
        {
            Undo.RecordObject(_rectangle, "Rectangle Manipulation");
            _rectangle.Width = EditorGUILayout.FloatField("Width", _rectangle.Width);
            _rectangle.Height = EditorGUILayout.FloatField("Height", _rectangle.Height);
            _rectangle.SetPoints();
            Undo.ClearUndo(_rectangle);
        }

        #endregion Inspector
    }
}