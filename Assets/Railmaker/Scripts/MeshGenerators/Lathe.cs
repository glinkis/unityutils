﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Railmaker {

    [Serializable]
    [AddComponentMenu("Railmaker/SplineMeshes/Lathe")]
    [RequireComponent(typeof (MeshFilter))]
    [RequireComponent(typeof (MeshRenderer))]
    public class Lathe : MeshGenerator {

        [SerializeField] private float _angle;

        private float _loopRotation;

        [SerializeField] private float _movement;

        [SerializeField] private Spline _spline;

        private List<Vector3> _splinePoints;

        private Vector3 _splinePosition;

        [SerializeField] private int _subdivisions;

        public Spline Spline {
            get { return _spline; }
            set {
                if (_spline == value) {
                    return;
                }
                if (_spline != null) {
                    _spline.PropertyChanged -= OnPropertyChanged;
                }
                if (value != null) {
                    value.PropertyChanged += OnPropertyChanged;
                }
                _spline = value;
                OnPropertyChanged("Spline");
            }
        }

        public float Angle {
            get { return _angle; }
            set {
                _angle = value;
                OnPropertyChanged("Angle");
            }
        }

        public int Subdivisions {
            get { return _subdivisions; }
            set {
                _subdivisions = value;
                OnPropertyChanged("Subdivisions");
            }
        }

        public float Movement {
            get { return _movement; }
            set {
                _movement = value;
                OnPropertyChanged("Movement");
            }
        }

        public override void Awake() {
            MeshName = "Lathe";
            Angle = 360f;
            Subdivisions = 16;
            Movement = 0f;
        }

        internal override void ResetMeshData() {
            if (_spline == null) {
                return;
            }

            _splinePosition = Spline.transform.localPosition;
            _splinePoints = Spline.GetInterpolatedPoints();
            PointCount = _splinePoints.Count;

            Vertices = new List<Vector3>();
            Uvs = new List<Vector2>();
        }

        internal override void RecalculateVertices() {
            for (int loop = 0; loop <= Subdivisions + 1; loop++) {
                GoThroughSplinePoints(loop);
            }
        }

        internal override void RecalculateIndices() {
            SewPolygonsBetweenEqualLoops(PointCount, Subdivisions);
        }

        private void GoThroughSplinePoints(int loop) {
            _loopRotation = (Angle/(Subdivisions + 1))*loop;
            Quaternion quat = Quaternion.AngleAxis(_loopRotation, Vector3.up);

            for (int pt = 0; pt < PointCount; pt++) {
                Vertices.Add(GetVertexPosition(quat, pt));
                Uvs.Add(GetUvPosition(loop, pt));
            }
        }

        private Vector3 GetVertexPosition(Quaternion quat, int pt) {
            Vector3 _movementOffset = Vector3.up*Movement*(1/Angle)*_loopRotation;
            return (quat*_splinePoints[pt]) + (quat*_splinePosition) + _movementOffset;
        }

        private Vector2 GetUvPosition(int loop, int pt) {
            float u = (1f/(PointCount - 1))*pt;
            float v = (1f/(Subdivisions + 1))*loop;
            return new Vector2(u, v);
        }

    }

}