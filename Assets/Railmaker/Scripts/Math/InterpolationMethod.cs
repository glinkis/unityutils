﻿using System;
using System.Linq;
using UnityEngine;

namespace Railmaker {

    public abstract class InterpolationMethod {

        public abstract float CalculateInterpolation(float t, float[] values);

        public abstract float CalculateDerivative(float t, float[] values);

        public Vector2 CalculateInterpolation(float t, Vector2[] values) {
            float x = CalculateInterpolation(t, values.Select(value => value.x).ToArray());
            float y = CalculateInterpolation(t, values.Select(value => value.y).ToArray());

            Vector2 val = new Vector2(x, y);
            return val;
        }

        public Vector3 CalculateInterpolation(float t, Vector3[] values) {
            float x = CalculateInterpolation(t, values.Select(value => value.x).ToArray());
            float y = CalculateInterpolation(t, values.Select(value => value.y).ToArray());
            float z = CalculateInterpolation(t, values.Select(value => value.z).ToArray());

            Vector3 val = new Vector3(x, y, z);
            return val;
        }

        public Vector4 CalculateInterpolation(float t, Vector4[] values) {
            float x = CalculateInterpolation(t, values.Select(value => value.x).ToArray());
            float y = CalculateInterpolation(t, values.Select(value => value.y).ToArray());
            float z = CalculateInterpolation(t, values.Select(value => value.z).ToArray());
            float w = CalculateInterpolation(t, values.Select(value => value.w).ToArray());

            Vector4 val = new Vector4(x, y, z, w);
            return val;
        }

        public Vector2 CalculateDerivative(float t, Vector2[] values) {
            float x = CalculateDerivative(t, values.Select(value => value.x).ToArray());
            float y = CalculateDerivative(t, values.Select(value => value.y).ToArray());

            Vector2 val = new Vector2(x, y);
            return val;
        }

        public Vector3 CalculateDerivative(float t, Vector3[] values) {
            float x = CalculateDerivative(t, values.Select(value => value.x).ToArray());
            float y = CalculateDerivative(t, values.Select(value => value.y).ToArray());
            float z = CalculateDerivative(t, values.Select(value => value.z).ToArray());

            Vector3 val = new Vector3(x, y, z);
            return val;
        }

        public Vector4 CalculateDerivative(float t, Vector4[] values) {
            float x = CalculateDerivative(t, values.Select(value => value.x).ToArray());
            float y = CalculateDerivative(t, values.Select(value => value.y).ToArray());
            float z = CalculateDerivative(t, values.Select(value => value.z).ToArray());
            float w = CalculateDerivative(t, values.Select(value => value.w).ToArray());

            Vector4 val = new Vector4(x, y, z, w);
            return val;
        }

        public sealed class Linear : InterpolationMethod {

            public override float CalculateInterpolation(float t, float[] values) {
                if (values.Length != 2) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 2 + ". Got " +
                                                          values.Length + ".");
                }
                return LinearInterpolation(t, values[0], values[1]);
            }

            public override float CalculateDerivative(float t, float[] values) {
                if (values.Length != 2) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 2 + ". Got " +
                                                          values.Length + ".");
                }
                return LinearDerivative(t, values[0], values[1]);
            }

            private float LinearInterpolation(float t, float value1, float value2) {
                float val = value1 + ((value2 - value1)*t);
                return val;
            }

            private float LinearDerivative(float t, float value1, float value2) {
                return 1.0f;
            }

        }

        public sealed class Quadratic : InterpolationMethod {

            public override float CalculateInterpolation(float t, float[] values) {
                if (values.Length != 3) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 3 + ". Got " +
                                                          values.Length + ".");
                }
                return QuadraticInterpolation(t, values[0], values[1], values[2]);
            }

            public override float CalculateDerivative(float t, float[] values) {
                if (values.Length != 3) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 3 + ". Got " +
                                                          values.Length + ".");
                }
                return QuadraticDerivative(t, values[0], values[1], values[2]);
            }

            private float QuadraticInterpolation(float t, float value1, float value2, float value3) {
                float a = 1 - t;

                float val = value1*a*a;
                val += value2*2*a*t;
                val += value3*t*t;

                return val;
            }

            private float QuadraticDerivative(float t, float value1, float value2, float value3) {
                float val = 2f*(1f - t)*(value2 - value1);
                val += 2f*t*(value3 - value2);

                return val;
            }

        }

        public sealed class Cubic : InterpolationMethod {

            public override float CalculateInterpolation(float t, float[] values) {
                if (values.Length != 4) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 4 + ". Got " +
                                                          values.Length + ".");
                }
                return CubicInterpolation(t, values[0], values[1], values[2], values[3]);
            }

            public override float CalculateDerivative(float t, float[] values) {
                if (values.Length != 4) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 4 + ". Got " +
                                                          values.Length + ".");
                }
                return CubicDerivative(t, values[0], values[1], values[2], values[3]);
            }

            private float CubicInterpolation(float t, float value1, float value2, float value3, float value4) {
                float a = 1 - t;
                float b = t*t;
                float c = a*a;
                float d = c*a;
                float e = b*t;

                float val = value1*d;
                val += value2*3*c*t;
                val += value3*3*a*b;
                val += value4*e;

                return val;
            }

            private float CubicDerivative(float t, float value1, float value2, float value3, float value4) {
                t = Math.Min(Math.Max(t, 0), 1);

                float a = 1f - t;

                float val = 3f*a*a*(value2 - value1);
                val += 6f*a*t*(value3 - value2);
                val += 3f*t*t*(value4 - value3);

                return val;
            }

        }

        public sealed class CatmullRom : InterpolationMethod {

            public override float CalculateInterpolation(float t, float[] values) {
                if (values.Length != 4) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 4 + ". Got " +
                                                          values.Length + ".");
                }
                return CatmullRomInterpolation(t, values[0], values[1], values[2], values[3]);
            }

            public override float CalculateDerivative(float t, float[] values) {
                if (values.Length != 4) {
                    throw new ArgumentOutOfRangeException("Unexpected number of values. Expected " + 4 + ". Got " +
                                                          values.Length + ".");
                }
                return CatmullRomDerivative(t, values[0], values[1], values[2], values[3]);
            }

            private float CatmullRomInterpolation(float t, float value1, float value2, float value3, float value4) {
                float u3 = t*t*t;
                float u2 = t*t;

                float f1 = -0.5f*u3 + u2 - 0.5f*t;
                float f2 = 1.5f*u3 - 2.5f*u2 + 1.0f;
                float f3 = -1.5f*u3 + 2.0f*u2 + 0.5f*t;
                float f4 = 0.5f*u3 - 0.5f*u2;

                float val = value1*f1 + value2*f2 + value3*f3 + value4*f4;

                return val;
            }

            private float CatmullRomDerivative(float t, float value1, float value2, float value3, float value4) {
                float val = value2 + 2*value3*t + 3*value4*(t*t);
                return val;
            }

        }

    }

}