using System;

namespace Railmaker
{
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Spline), true)]
    [CanEditMultipleObjects]
    public abstract class SplineEditor : Editor
    {
        internal Spline Spline { get; set; }

        internal Spline[] Splines { get; set; }

        public void OnEnable()
        {
            Splines = targets.Cast<Spline>().ToArray();
            Spline = Splines[0];
        }

        public override void OnInspectorGUI()
        {
            TitleGui();
            AttributesGui();
            SceneView.RepaintAll();
        }

        public void OnSceneGUI()
        {
            foreach (Spline s in Splines)
                DrawEditableSpline(s);
            Spline.SetPoints();
        }

        #region Inspector

        private void TitleGui()
        {
            GUIStyle style = new GUIStyle { fontStyle = FontStyle.Bold };
            EditorGUILayout.LabelField("Spline", style);
        }

        private void AttributesGui()
        {
            Undo.RecordObject(Spline, "Spline Manipulation");
            PointCountGui();
            IsClosedGui();
            IntermediatePointsNumberGui();
            EditorGUILayout.Space();
            Undo.ClearUndo(Spline);
        }

        private void PointCountGui()
        {
            string controlPoints = Spline.ControlPoints.Count + " control points. ";
            string interpolatedPoints = Spline.GetInterpolatedPoints().Count + " generated points.";
            EditorGUILayout.LabelField(controlPoints + interpolatedPoints);
        }

        private void IsClosedGui()
        {
            Spline.IsClosed = EditorGUILayout.Toggle("IsClosed Curve", Spline.IsClosed);
        }

        private void IntermediatePointsNumberGui()
        {
            Spline.IntermediatePointsNumber = EditorGUILayout.IntField("Number", Spline.IntermediatePointsNumber);
            if (Spline.IntermediatePointsNumber < 0)
                Spline.IntermediatePointsNumber = 0;
        }

        #endregion Inspector

        #region Scene

        private void DrawEditableSpline(Spline s)
        {
            Undo.RecordObject(s, "Spline Modification");
            Handles.matrix = s.transform.localToWorldMatrix;
            DrawIntermediatePoints(s);
            DrawControlPoints(s);
            Undo.ClearUndo(s);
        }

        private void DrawControlPoints(Spline s)
        {
            foreach (Point p in s.ControlPoints)
                DrawPoint(p);
        }

        private void DrawIntermediatePoints(Spline s)
        {
            var pts = s.GetInterpolatedPoints().ToArray();

            Handles.color = Color.blue;
            Handles.DrawPolyLine(pts);

            foreach (Vector3 pt in pts) {
                Handles.DotCap(0, pt, new Quaternion(), HandleUtility.GetHandleSize(pt) * 0.02f);
            }
        }

        private void DrawPointNumbers(Spline s)
        {
            for (int i = 0; i < s.ControlPoints.Count; i++)
            {
                GUIStyle style = new GUIStyle
                {
                    fontSize = 12,
                    fontStyle = FontStyle.Bold,
                    normal = { textColor = Color.white }
                };
                Handles.Label(s.ControlPoints[i].Position, i.ToString(), style);
            }
        }
        public void DrawPoint(Point p)
        {
            Color pointColor = new Color(80 / 255f, 161 / 255f, 207 / 255f, 1);

            p.Position = SimpleFreeMoveHandle(p.Position, pointColor);
            DrawTangentsInEditor(p);
        }

        public void DrawTangentsInEditor(Point p)
        {
            Color tangentColor = new Color(80 / 255f, 161 / 255f, 207 / 255f, 1);
            Color lineColor = new Color(80 / 255f, 161 / 255f, 207 / 255f, 1);

            Handles.color = lineColor;
            Handles.DrawLine(p.LeftTangentAbsolute, p.Position);
            p.LeftTangentAbsolute = SimpleFreeMoveHandle(p.LeftTangentAbsolute, tangentColor);

            Handles.color = lineColor;
            Handles.DrawLine(p.RightTangentAbsolute, p.Position);
            p.RightTangentAbsolute = SimpleFreeMoveHandle(p.RightTangentAbsolute, tangentColor);
        }

        public void DrawPointPositionInEditor(Point p)
        {
            GUIStyle style = new GUIStyle
            {
                fontSize = 12,
                fontStyle = FontStyle.Bold,
                normal = { textColor = Color.white }
            };
            Handles.Label(p.Position, p.Position.ToString(), style);
        }

        private Vector3 SimpleFreeMoveHandle(Vector3 pos, Color color)
        {
            Handles.color = color;
            float size = HandleUtility.GetHandleSize(pos) * 0.03f;
            return Handles.FreeMoveHandle(pos, Quaternion.identity, size, Vector3.zero, Handles.DotCap);
        }

        #endregion Scene
    }
}